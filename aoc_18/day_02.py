from collections import defaultdict
from itertools import compress


def part_a(box_ids):
    two_letters = 0
    three_letters = 0

    for bid in box_ids:
        letter_cnt = defaultdict(int)
        for l in bid:
            letter_cnt[l] += 1

        cnts = set(letter_cnt.values())
        two_letters += 2 in cnts
        three_letters += 3 in cnts

    return two_letters * three_letters


def part_b(box_ids):

    for i, bid in enumerate(box_ids):
        remaining_bids = box_ids[i + 1 :]
        for other_bid in remaining_bids:
            diff = [l1 == l2 for l1, l2 in zip(list(bid), list(other_bid))]
            if sum(diff) == len(bid) - 1:
                return "".join(compress(bid, diff))


if __name__ == "__main__":
    with open("day_02_input.txt") as f:
        box_ids = f.read().strip().splitlines()

    print(part_a(box_ids))
    print(part_b(box_ids))
