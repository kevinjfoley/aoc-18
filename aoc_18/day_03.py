from collections import defaultdict


def part_a(claims):

    cloth_claims = _get_cloth_claims(claims)
    overlap = 0
    for c in cloth_claims.values():
        overlap += len(c) > 1

    return overlap


def part_b(claims):

    cloth_claims = _get_cloth_claims(claims)
    id_overlaps = defaultdict(int)

    for c in cloth_claims.values():
        for id in c:
            id_overlaps[id] += len(c) > 1

    for id, overlaps in id_overlaps.items():
        if overlaps == 0:
            return id


def _parse_claim(raw_claim):

    id = int(raw_claim.split("#")[1].split("@")[0].strip())

    offset = raw_claim.split("@")[1].split(":")[0].strip()
    x_off, y_off = map(int, offset.split(","))

    size = raw_claim.split(":")[1].strip()
    width, height = map(int, size.split("x"))

    return id, x_off, y_off, width, height


def _get_cloth_claims(claims):

    cloth_claims = defaultdict(list)

    for raw_claim in claims:
        id, x_off, y_off, width, height = _parse_claim(raw_claim)
        for x in range(x_off, x_off + width):
            for y in range(y_off, y_off + height):
                cloth_claims[(x, y)].append(id)

    return cloth_claims


if __name__ == "__main__":
    with open("day_03_input.txt") as f:
        claims = f.read().strip().splitlines()

    print(part_a(claims))
    print(part_b(claims))
