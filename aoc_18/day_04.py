import re
from datetime import datetime as dt, timedelta
from collections import defaultdict


def part_a(guard_log):

    log = _parse_log(guard_log)
    sleep_schedule = get_sleep_schedule(log)

    total_sleep = {}
    for g, ss in sleep_schedule.items():
        total_sleep[g] = sum(ss.values())

    max_sleep_guard = max(total_sleep, key=total_sleep.get)
    max_minute = max(
        sleep_schedule[max_sleep_guard], key=sleep_schedule[max_sleep_guard].get
    )

    return max_sleep_guard * max_minute


def part_b(guard_log):

    log = _parse_log(guard_log)
    sleep_schedule = get_sleep_schedule(log)

    guard_max_min = {g: max(ss, key=ss.get) for g, ss in sleep_schedule.items()}

    max_guard = max(guard_max_min, key=lambda x: sleep_schedule[x][guard_max_min[x]])

    return max_guard * guard_max_min[max_guard]


def get_sleep_schedule(log):

    sleep_schedule = defaultdict(lambda: defaultdict(int))
    for ts, action in log:
        if action.split(" ")[0] == "Guard":
            guard = int(re.search("#(\S+)", action).group(1))
        elif action == "falls asleep":
            sleep_start = ts
        elif action == "wakes up":
            min_asleep = (ts - sleep_start).seconds / 60
            for m in range(int(min_asleep)):
                sleep_schedule[guard][(sleep_start + timedelta(minutes=m)).minute] += 1

    return sleep_schedule


def _parse_log(guard_log):
    """Return a time sorted list of (time, action) tuples
    """

    log = []
    for l in guard_log:
        ts_str, action = re.search("\[(.+)\] (.+)", l).group(1, 2)
        ts = dt.strptime(ts_str, "%Y-%m-%d %H:%M")
        log.append((ts, action))

    return sorted(log, key=lambda x: x[0])


if __name__ == "__main__":
    with open("day_04_input.txt") as f:
        log = f.read().strip().splitlines()

    print(part_a(log))
    print(part_b(log))
