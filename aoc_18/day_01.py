def part_a(frequency_changes):

    freq_deltas = map(int, frequency_changes.split(","))
    return sum(freq_deltas)


def part_b(frequency_changes, cur_freq=0, prev_freqs=None):

    if not prev_freqs:
        prev_freqs = set([0])
    freq_deltas = map(int, frequency_changes.split(","))

    for delt in freq_deltas:
        cur_freq = sum([cur_freq, delt])
        if cur_freq in prev_freqs:
            return cur_freq
        else:
            prev_freqs.add(cur_freq)

    return part_b(frequency_changes, cur_freq, prev_freqs)


if __name__ == "__main__":
    with open("day_01_input.txt") as f:
        day_01_input = f.read().strip().replace("\n", ",")

    print(part_a(day_01_input))

    print(part_b(day_01_input))
