import unittest
from aoc_18.day_03 import part_a, part_b, _parse_claim


class TestDay03(unittest.TestCase):
    def test_part_a(self):
        """Test part a example
        """
        claims = ["#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"]

        self.assertEqual(part_a(claims), 4)

    def test_part_b(self):

        claims = ["#1 @ 1,3: 4x4", "#2 @ 3,1: 4x4", "#3 @ 5,5: 2x2"]

        self.assertEqual(part_b(claims), 3)

    def test__parse_claim(self):

        claim = "#123 @ 3,2: 5x4"

        self.assertTupleEqual(_parse_claim(claim), (123, 3, 2, 5, 4))
