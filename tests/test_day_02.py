import unittest
from aoc_18.day_02 import part_a, part_b


class TestDay02(unittest.TestCase):
    def test_part_a(self):
        """Test part a example
        """
        box_ids = ["abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab"]

        self.assertEqual(part_a(box_ids), 12)

    def test_part_b(self):
        box_ids = ["abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz"]

        self.assertEqual(part_b(box_ids), "fgij")


if __name__ == "__main__":
    unittest.main()
