import unittest
from aoc_18.day_01 import part_a, part_b


class TestDay01(unittest.TestCase):
    def test_part_a(self):
        """Test part a examples
        """
        examples = [
            ("+1, -2, +3, +1", 3),
            ("+1, +1, +1", 3),
            ("+1, +1, -2", 0),
            ("-1, -2, -3", -6),
        ]

        for ex in examples:
            with self.subTest(puzzle_input=ex[0]):
                self.assertEqual(part_a(ex[0]), ex[1])

    def test_part_b(self):
        """Test part a examples
        """
        examples = [
            ("+1, -1", 0),
            ("+3, +3, +4, -2, -4", 10),
            ("-6, +3, +8, +5, -6", 5),
            ("+7, +7, -2, -7, -4", 14),
        ]

        for ex in examples:
            with self.subTest(puzzle_input=ex[0]):
                self.assertEqual(part_b(ex[0]), ex[1])


if __name__ == "__main__":
    unittest.main()
